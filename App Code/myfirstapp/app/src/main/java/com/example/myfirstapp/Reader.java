package com.example.myfirstapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import static com.example.myfirstapp.DisplayMessageActivity.NAME_SESSION;
import static com.example.myfirstapp.DisplayMessageActivity.input;
import static com.example.myfirstapp.DisplayMessageActivity.name;
import static com.example.myfirstapp.DisplayMessageActivity.out;
import static com.example.myfirstapp.DisplayMessageActivity.session_ID;
import static com.example.myfirstapp.DisplayMessageActivity.socket;
import static com.example.myfirstapp.RecordData.startButton;
import static com.example.myfirstapp.RecordData.stopButton;

public class Reader extends AsyncTask< String, Void, Integer> {
    private ProgressDialog p;

    Activity activity;

    public Reader(DisplayMessageActivity act) {
        this.activity = act ;
        p = new ProgressDialog(activity);
    }
    public Reader(RecordData act) {
        this.activity = act ;
        p = new ProgressDialog(activity);
    }

    protected Integer doInBackground(String... message) {
        // establish a connection
        try {
            if (message[0].equals("config")){p.setMessage("Programming Sensors");}
            else if (message[0].equals("close")){p.setMessage("Resetting Sensors");}
            else if (message[0].equals("start")){p.setMessage("Starting Recording");}    //This shouldn't show up but I have put it just in case
            else if (message[0].equals("stop")){p.setMessage("Stopping Recording");}
            else{p.setMessage("Communicating with Server");}

            if(message.length==2) {//Sending Name of File
                out.writeUTF(message[0]);
                out.writeUTF(message[1]);
            }
            else{
                out.writeUTF(message[0]);
            }
            String line = input.readLine();
            System.out.println(line); //System.out.println
            System.out.println(line.equals("true"));
            if (line.equals("true") && message[0].equals("config")){return 5;}
            else if (line.equals("true") && message[0].equals("close")){return 4;}
            else if (line.equals("true") && message[0].equals("start")){return 3;}
            else if (line.equals("true") && message[0].equals("stop")){return 2;}
            else if (line.equals("true")){return 1;}
            else{return 0;}

        } catch (UnknownHostException u) {
            System.out.println(u);
            return 0;
        } catch (Exception i) {
            System.out.println(i);
            return 0;
        }
    }

    // This is called when doInBackground() is finished
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        if (result == 5) {  //Sent Config
            System.out.println("Move From Display.. to Record..");

            //Toast.makeText(this, "Starting Recording" , Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this.activity, RecordData.class);
            String session = session_ID.getText().toString();
            String message = name + "_" + session;
            Log.d("NAME" ,message);
            intent.putExtra(NAME_SESSION, message);
            activity.startActivity(intent);
        }
        else if (result == 4) { //Sent Close
            System.out.println("Move From Record.. to Display..");
            Intent myIntent = new Intent(this.activity, DisplayMessageActivity.class);
            activity.startActivityForResult(myIntent, 0);
        }
        else if (result == 3) {//Sent Start
            System.out.println("Started Recording");
            startButton.setEnabled(false);
            stopButton.setEnabled(true);
        }
        else if (result == 2) { //Sent Stop
            System.out.println("Finished Recording");
            startButton.setEnabled(true);
            stopButton.setEnabled(false);
        }
        else if (result == 1) { //Sent a message
            System.out.println("Communication was Successful");
        }
        else {
            p.setMessage("An Error Occured");
            System.out.println("ERROR Occured");
            try{Thread.sleep(5000);}
            catch (Exception e){System.out.println(e);}

        }
        p.hide();
        //return null;
    }

    protected void onPreExecute() {
        super.onPreExecute();
        //p.setMessage("Reading");
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.show();
    }
}
