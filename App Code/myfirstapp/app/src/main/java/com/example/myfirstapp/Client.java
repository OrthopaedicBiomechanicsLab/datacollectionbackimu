package com.example.myfirstapp;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import static com.example.myfirstapp.DisplayMessageActivity.socket;
import static com.example.myfirstapp.DisplayMessageActivity.input;
import static com.example.myfirstapp.DisplayMessageActivity.out;

public class Client extends AsyncTask< Void, Void, String> {
    // Do the long-running work in here
    // initialize socket and input output stream
    //public Socket socket //		 = null;
    //public DataInputStream input //= null;
    //public DataOutputStream out //	 = null;
    private ProgressDialog p;

    public Client(DisplayMessageActivity activity) {
        p = new ProgressDialog(activity);
    }
    protected String doInBackground(Void... voids) {
        // establish a connection
        try
        {
            socket = new Socket("192.168.4.1", 5000); //localhost
            //Toast.makeText(this, "Establishing Socket" , Toast.LENGTH_LONG).show();

            // takes input from terminal
            input = new BufferedReader(new InputStreamReader(socket.getInputStream())); //was: System.in

            //input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // sends output to the socket
            out = new DataOutputStream(socket.getOutputStream());

        }
        catch(UnknownHostException u)
        {
            System.out.println(u);
        }
        catch(IOException i)
        {
            System.out.println(i);
        }
        return "DONE";
    }

    // This is called when doInBackground() is finished
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        System.out.println("FINISHED ESTABLISHING SOCKET");
        p.hide();
        //return null;
    }
    protected void onPreExecute() {
        super.onPreExecute();
        p.setMessage("Please wait...It is establishing a socket");
        p.setIndeterminate(false);
        p.setCancelable(false);
        p.show();
    }
}