package com.example.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;

import static com.example.myfirstapp.DisplayMessageActivity.hideKeyboardFrom;
import static com.example.myfirstapp.DisplayMessageActivity.out;
import static com.example.myfirstapp.DisplayMessageActivity.input;

public class RecordData extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public Integer result;
    public static Button startButton;
    public static Button stopButton;
    public static String message;
    public static String exercise_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Avoid the keyboard from popping up when it first enters here
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_data);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        message = intent.getStringExtra(DisplayMessageActivity.NAME_SESSION);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.session_name);
        textView.setText(message);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        // Enable and Disable Buttons
        startButton = findViewById(R.id.start_button);
        stopButton = findViewById(R.id.stop_button);

        startButton.setEnabled(true);
        stopButton.setEnabled(false);

    }

    public void startRecord(View view) {
        // Do something in response to button
        // string to read message from input
        hideKeyboardFrom(this,view);
        // Do something in response to button
        //EditText exercise = (EditText) findViewById(R.id.exercise);
        //String exercise_name = exercise.getText().toString();

        String file_name = message + "_" + exercise_name;
       try
        {
            new Reader(RecordData.this).execute("name",file_name);
 /*           if (result) {
                System.out.println("Saved Name Correctly");
            }*/

        }
        catch(Exception i)
        {
            System.out.println(i);
        }
        try
        {
            //Adding 'get' to remove the prompt from showing so that user can press stop
            (new Reader(RecordData.this).execute("start")).get();

        }
        catch(Exception i)
        {
            System.out.println(i);
        }

    }

    public void stopRecord(View view) {
        // Do something in response to button
        try
        {
            //line = input.readLine();
            //out.writeUTF("stop");
            new Reader(RecordData.this).execute("stop");
/*            if (result) {
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
            }*/
        }
        catch(Exception i)
        {
            System.out.println(i);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        // Do something in response to button
        //Resetting all the devices
        try
        {
            //line = input.readLine();
            //out.writeUTF("close");
            new Reader(RecordData.this).execute("close");
        }
        catch(Exception i)
        {
            System.out.println(i);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        try
        {
            //line = input.readLine();
            //out.writeUTF("close");
            new Reader(RecordData.this).execute("close");
/*            if (result) {
                System.out.println("BACKKKKKKKKK222222");
            }
            else{
                System.out.println("Something Went Wrong");
            }*/

        }
        catch(Exception i)
        {
            System.out.println(i);
        }
        return true;
    }


    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        System.out.println((parent.getItemAtPosition(pos)).toString());
        exercise_name = (parent.getItemAtPosition(pos)).toString();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
        System.out.println(parent.getItemAtPosition(0));
        exercise_name = "None";
    }
}