package com.example.myfirstapp;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.MacAddress;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.NetworkSpecifier;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiNetworkSpecifier;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PatternMatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Fragment;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import static java.lang.Thread.sleep;



public class DisplayMessageActivity extends AppCompatActivity {
    public static final String NAME_SESSION = "com.example.myfirstapp.MESSAGE";
    public static EditText session_ID ;
    public static String name;
    public Boolean result;

    // initialize socket and input output streams
    public static Socket socket		 = null;
    public static BufferedReader input = null; //DataInputStream
    public static DataOutputStream out	 = null;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Avoid the keyboard from popping up when it first enters here
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //The initialization of the activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        name = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.textView);
        textView.setText(name);

        //Setting the public static variable for the session id to the new created text editor
        session_ID = (EditText) findViewById(R.id.messageC);

        //Extract recycle list info
        recyclerView = (RecyclerView) findViewById(R.id.devices);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new GridLayoutManager(this, 3); //LinearLayoutManager(this);
        //layoutManager = (LinearLayoutManager) findViewById(R.id.linearLayout);
        recyclerView.setLayoutManager(layoutManager);

        String[] myDataset = {"Volvo", "BMW", "Ford", "Mazda"};

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(myDataset);
        recyclerView.setAdapter(mAdapter);


        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo ();
        String current_ssid  = info.getSSID();
        Log.d("WIFI NET" ,current_ssid);
        if (current_ssid.equals("\""+"abdo"+"\"")){
            Log.d("WIFI NET" ,"GOOD");
        }
        else {
            Log.d("WIFI NET", "BAD");
            //Attempt to Connect to the RPi network
            try {
                String networkSSID = "abdo";
                String networkPass = "Abdalr1997";
/*
            final NetworkSpecifier specifier = new WifiNetworkSpecifier.Builder()
                    .setSsid(networkSSID) //new PatternMatcher(networkSSID, android.os.PatterMatcher.PATTERN_PREFIX)
                    //.setBssidPattern(MacAddress.fromString("10:03:23:00:00:00"), MacAddress.fromString("ff:ff:ff:00:00:00"))
                    .setWpa2Passphrase(networkPass)
                    .build();
            final NetworkRequest request = new NetworkRequest.Builder()
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .removeCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .setNetworkSpecifier(specifier)
                    .build();
            final ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            final ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback();
            connectivityManager.requestNetwork(request, networkCallback);

*/


                WifiConfiguration conf = new WifiConfiguration();
                conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain SSID in quotes

                conf.preSharedKey = "\"" + networkPass + "\"";

                conf.status = WifiConfiguration.Status.ENABLED;
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);

                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);


                wifiManager.setWifiEnabled(true);


                int networkId = wifiManager.addNetwork(conf);
                Log.d("ADD", String.valueOf(networkId));
                Log.d("after connecting", conf.SSID + " " + conf.preSharedKey);

                conf.SSID = networkSSID;
                conf.preSharedKey = networkPass;

                wifiManager.startScan();

                List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
                //Log.d("LIST: ", String.valueOf(list));
                for (WifiConfiguration i : list) { //ScanResult i : wifiManager.getScanResults()
                    //Log.d("THE WIFI: ", String.valueOf(i));
                    Log.d("SSID  ", String.valueOf(i.SSID));
                    Log.d("value  ", String.valueOf(i.SSID.equals("\"" + networkSSID + "\"")));//String.valueOf(networkSSID==String.valueOf(i.SSID))); //String.valueOf(String.valueOf()));
                    if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                        //int networkId = wifiManager.addNetwork(conf);
                        Log.d("ID", String.valueOf(networkId));
                        if (networkId == -1) {
                            // Get existed network id if it is already added to WiFi network
                            networkId = getExistingNetworkId(i.SSID);
                            Log.d("newID", "getExistingNetworkId: " + networkId);
                        }
                        if (networkId != -1) {

                            wifiManager.disconnect();
                            wifiManager.enableNetwork(i.networkId, true);
                            wifiManager.reconnect();
                            Log.d("re connecting", i.SSID + " " + conf.preSharedKey);
                            Toast.makeText(this, "OK " + i.SSID, Toast.LENGTH_LONG).show();
                            Toast.makeText(this, "Network ID  " + i.networkId, Toast.LENGTH_LONG).show();
                        } else {
                            Log.d("Nope", "Nope");
                        }
                        break;
                    }
                }

                //WiFi Connection success, return true
                //return true;
                //textView.setText("Connection!!");
                Log.d("Connection", "Connected");

            } catch (Exception ex) {
                System.out.println(Arrays.toString(ex.getStackTrace()));
                //return false;
                //textView.setText("NOOO CONNNECTION");
                Log.d("Coonection", "Unable to Connect");

            }
        }
    }

    private int getExistingNetworkId(String SSID) {
        WifiManager wifiManager = (WifiManager) super.getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        if (configuredNetworks != null) {
            for (WifiConfiguration existingConfig : configuredNetworks) {
                if (existingConfig.SSID.equals(SSID)) {
                    return existingConfig.networkId;
                }
            }
        }
        return -1;
    }


    public void startScan(View view) {
        // Do something in response to button
        Toast.makeText(this, "Scanning" , Toast.LENGTH_LONG).show();
        try {

        }
        catch (Exception ex) {
            System.out.println(ex);
        }
    }


    public void startConnect(View view) {
        // Do something in response to button
        Toast.makeText(this, "Connecting" , Toast.LENGTH_LONG).show();
        String address = "192.168.4.1";
        int port = 5000;
        Client socketClient = new Client(DisplayMessageActivity.this);
        socketClient.execute();

        // sends output to the socket
        //out = new DataOutputStream(socket.getOutputStream());

        // Client client = new Client("192.168.4.1", 5000); // "127.0.0.1	"
    }

    public void clientMessage(View view) {
        hideKeyboardFrom(this,view);
        // Do something in response to button
        EditText editText = (EditText) findViewById(R.id.messageC);
        String message = editText.getText().toString();
        System.out.println("Got Message");
        // string to read message from input
        String line = "";

        try
        {
            //line = input.readLine();
            //System.out.println(input.readUTF());
            new Reader(DisplayMessageActivity.this).execute(message);
            //Log.d("INPUT", );
        }
        catch(Exception i)
        {
            System.out.println(i);
        }

    }



    public void startCode(View view) {
        try
        {
            new Reader(DisplayMessageActivity.this).execute("config");

        }
        catch(Exception i)
        {
            System.out.println(i);
        }
        /* try
        {
            //line = input.readLine();
            out.writeUTF("code");
        }
        catch(IOException i)
        {
            System.out.println(i);
        }*/
    }

    public void disconnectCode(View view) {

        // close the connection
        try
        {
            out.writeUTF("quit");
            input.close();
            out.close();
            socket.close();
            Toast.makeText(this, "Connection Disconnected" , Toast.LENGTH_LONG).show();
            System.out.println("DISCONNECTED SAFELY");
        }
        catch(IOException i)
        {
            System.out.println(i);
            System.out.println("Error");
        }

    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

/*    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        System.out.println("BACKKKKKKKKK");
        return true;
    }*/

}
