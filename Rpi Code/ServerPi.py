# TCP server example
# usage: python stream_acc.py [mac1] [mac2] ... [mac(n)]
from __future__ import print_function
from ctypes import c_void_p, cast, POINTER
from mbientlab.metawear import MetaWear, libmetawear, parse_value, cbindings, create_voidp, create_voidp_int
from mbientlab.metawear.cbindings import *
from mbientlab.warble import * 
from time import sleep
from threading import Event

import threading
import time, datetime, csv, signal
from datetime import datetime

import platform
import sys
import csv 

import os
import fcntl
import subprocess

import select
import socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("", 5000))
server_socket.listen(5)

def create_folder(path):
    #Create folder directory
    """Change the owner of the file to SUDO_UID"""

    if not os.path.exists(path):
        os.makedirs(path, mode=0o777)

    uid = os.environ.get('SUDO_UID')
    gid = os.environ.get('SUDO_GID')
    if uid is not None:
        os.chown(path, int(uid), int(gid))


def get_bl():
    """
        Gets the devfs path to a Teensy microcontroller by scraping the output
        of the lsusb command
        
        The lsusb command outputs a list of USB devices attached to a computer
        in the format:
            Bus 002 Device 009: ID 16c0:0483 Van Ooijen Technische Informatica Teensyduino Serial
        The devfs path to these devices is:
            /dev/bus/usb/<busnum>/<devnum>
        So for the above device, it would be:
            /dev/bus/usb/002/009
        This function generates that path.
    """
    proc = subprocess.Popen(['lsusb'], stdout=subprocess.PIPE,universal_newlines=True)
    out = proc.communicate()[0]
    lines = out.split('\n')
    for line in lines:
        if 'Cambridge' in line:
            parts = line.split()
            bus = parts[1]
            dev = parts[3][:3]
            return '/dev/bus/usb/%s/%s' % (bus, dev)


def send_reset(dev_path):
    """
        Sends the USBDEVFS_RESET IOCTL to a USB device.
        
        dev_path - The devfs path to the USB device (under /dev/bus/usb/)
                   See get_teensy for example of how to obtain this.
    """
    fd = os.open(dev_path, os.O_WRONLY)
    try:
        fcntl.ioctl(fd, USBDEVFS_RESET, 0) #For External BL Module - hci0
        #proc2 = subprocess.run(['sudo', 'hciconfig', 'hci1', 'reset']) #For Internal Bl Module - hci1
        #print("%s" %proc2)
    finally:
        os.close(fd)
        print("Finished Reset")


def reset_bl():
    """
        Finds a teensy and reset it.
    """
    send_reset(get_bl())


class myThread (threading.Thread):
   def __init__(self, name, state,method):
      threading.Thread.__init__(self)
      #self.threadID = threadID
      self.name = name
      self.s = state
      self.method = method
   def run(self):
      print ("Starting " + self.name)
      # Get lock to synchronize threads
      #threadLock.acquire()
      self.method(self.s)
      # Free lock to release next thread
      #threadLock.release()
      print ("Finished " + self.name + "\n")


class State:
    def __init__(self, device):
        self.device = device
        #self.samples = 0
        self.acc_samples = 0
        self.gyro_samples = 0
        self.mag_samples = 0
        self.pres_samples = 0

        self.acc_start_time = 0
        self.gyro_start_time = 0
        self.mag_start_time = 0
        self.pres_start_time = 0

        self.acc = None
        self.gyro = None
        self.mag = None
        self.pres = None

        self.battery_callback = FnVoid_VoidP_DataP(self.battery_data_handler)
        #self.callback = cbindings.FnVoid_VoidP_DataP(self.data_handler)
        self.acc_callback = FnVoid_VoidP_DataP(self.acc_data_handler)
        self.gyro_callback = FnVoid_VoidP_DataP(self.gyro_data_handler)
        self.mag_callback = FnVoid_VoidP_DataP(self.mag_data_handler)
        self.pres_callback = FnVoid_VoidP_DataP(self.pres_data_handler)
        self.processor = None

        self.acc_csv_file = None
        self.acc_csv_writer = None
        self.gyro_csv_file =None
        self.gyro_csv_writer = None
        self.mag_csv_file = None
        self.mag_csv_writer = None
        self.pres_csv_file = None
        self.pres_csv_writer = None



         
        '''
    def data_handler(self, ctx, data):
        values = parse_value(data,n_elem = 2)
        #print("acc: (%.4f,%.4f,%.4f), gyro; (%.4f,%.4f,%.4f), mag; (%.4f,%.4f,%.4f)"  % (values[0].x, values[0].y, values[0].z, values[1].x, values[1].y, values[1].z, values[2].x, values[2].y, values[2].z,))
        self.samples+= 1
        print("%s" % values)
        print ("GAP")
        #print("%s" % data[100])
        #(values[0].x, values[0].y, values[0].z, values[1].x, values[1].y, values[1].z, values[2].x, values[2].y, values[2].z,)
        '''
    def led(self):
        pattern= LedPattern(repeat_count= Const.LED_REPEAT_INDEFINITELY)
        libmetawear.mbl_mw_led_load_preset_pattern(byref(pattern), LedPreset.PULSE)
        libmetawear.mbl_mw_led_write_pattern(self.device.board, byref(pattern), LedColor.GREEN)
        libmetawear.mbl_mw_led_play(self.device.board)

        sleep(2.0)

        libmetawear.mbl_mw_led_stop_and_clear(self.device.board)
        sleep(1.0)

    def acc_data_handler(self, ctx, data):
        values = parse_value(data)
        #print("%s -> Acc -> %s" % (self.device.address, values))
        now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')
        epoch = str(data.contents.epoch)
        
        if (self.acc_samples == 0):
            self.acc_start_time = data.contents.epoch
        elapsed = str((data.contents.epoch - self.acc_start_time)/1000)
        
        datastr = ("%s,%s,%s" %(values.x,values.y,values.z))
        data="".join(datastr)
        datarow=epoch+","+now+","+elapsed+","+data
        e = Event()
        e.set()

        self.acc_csv_writer.writerow([datarow])
        e.wait()
        self.acc_samples+= 1

 
    def gyro_data_handler(self, ctx, data):
        values = parse_value(data)
        #print("%s -> Gyro -> %s" % (self.device.address, values))
        now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')
        epoch = str(data.contents.epoch)

        if (self.gyro_samples == 0):
            self.gyro_start_time = data.contents.epoch
        elapsed = str((data.contents.epoch - self.gyro_start_time)/1000)

        datastr = ("%s,%s,%s" %(values.x,values.y,values.z))
        data="".join(datastr)
        datarow=epoch+","+now+","+elapsed+","+data
        e = Event()
        e.set()
        self.gyro_csv_writer.writerow([datarow])
        e.wait()

        self.gyro_samples+= 1

    def mag_data_handler(self, ctx, data):
        values = parse_value(data)
        #print("%s -> Mag -> %s" % (self.device.address, values))
        now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')
        epoch = str(data.contents.epoch)
        
        if (self.mag_samples == 0):
            self.mag_start_time = data.contents.epoch
        elapsed = str((data.contents.epoch - self.mag_start_time)/1000)

        datastr = ("%s,%s,%s" %(values.x,values.y,values.z))
        data="".join(datastr)
        datarow=epoch+","+now+","+elapsed+","+data
        e = Event()
        e.set()
        self.mag_csv_writer.writerow([datarow])

        e.wait()

        self.mag_samples+= 1

    def pres_data_handler(self, ctx, data):
        values = parse_value(data)
        #print("%s -> Pres -> %s" % (self.device.address, values))
        now = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')
        epoch = str(data.contents.epoch)
        
        if (self.pres_samples == 0):
            self.pres_start_time = data.contents.epoch
        elapsed = str((data.contents.epoch - self.pres_start_time)/1000)

        datastr = ("%s" %(values))
        data="".join(datastr)
        datarow=epoch+","+now+","+elapsed+","+data
        e = Event()
        e.set()
        self.pres_csv_writer.writerow([datarow])

        e.wait()
        self.pres_samples+= 1

    def battery_data_handler(self,ctx,data):
        values = parse_value(data)
        print("%s -> Battery -> %s" % (self.device.address, values))


    def config(self):

        #First set the disconnection handler to deatl with disconnects
        self.device.on_disconnect = lambda s: self.disconnect_handler()

        #Reset Board
        libmetawear.mbl_mw_logging_stop(self.device.board)
        libmetawear.mbl_mw_logging_clear_entries(self.device.board)
        libmetawear.mbl_mw_macro_erase_all(self.device.board)
        libmetawear.mbl_mw_debug_reset_after_gc(self.device.board)  #libmetawear.mbl_mw_debug_reset_after_gc(self.device.board)
        libmetawear.mbl_mw_metawearboard_tear_down(self.device.board)

        libmetawear.mbl_mw_settings_set_ad_parameters(self.device.board, 1000, 0, 0)
        #print("Erase logger, state, and macros")
        libmetawear.mbl_mw_settings_set_tx_power(self.device.board, 4)
        #Board Configuration
        libmetawear.mbl_mw_settings_set_connection_parameters(self.device.board, 7.5, 7.5, 0, 6000)
        sleep(1.5)
        #def processor_created(context, pointer):
        #    self.processor = pointer
        #    e.set()
        #fn_wrapper = cbindings.FnVoid_VoidP_VoidP(processor_created)

        #Sensor Configuration
        #Accelerometer
        libmetawear.mbl_mw_acc_set_odr(self.device.board, 25.0)
        libmetawear.mbl_mw_acc_set_range(self.device.board, 16.0)
        libmetawear.mbl_mw_acc_write_acceleration_config(self.device.board)

        #Gyro
        libmetawear.mbl_mw_gyro_bmi160_set_odr(self.device.board, GyroBmi160Odr._25Hz) #6 = 25Hz
        libmetawear.mbl_mw_gyro_bmi160_set_range(self.device.board, GyroBmi160Range._125dps) 
        libmetawear.mbl_mw_gyro_bmi160_write_config(self.device.board)

        #Mag
        #libmetawear.mbl_mw_mag_bmm150_set_odr(s.device.board, 0) #0 = 10Hz
        libmetawear.mbl_mw_mag_bmm150_set_preset(self.device.board, MagBmm150Preset.ENHANCED_REGULAR) #2 = ENHANCED_REGULAR
        #libmetawear.mbl_mw_mag_bmm150_write_config(s.device.board)

        #Pressure
        libmetawear.mbl_mw_baro_bosch_set_oversampling(self.device.board, BaroBoschOversampling.ULTRA_HIGH) #5 = ULTRA_HIGH
        libmetawear.mbl_mw_baro_bosch_set_standby_time(self.device.board, BaroBme280StandbyTime._62_5ms) #1 = 62.5 ms
        libmetawear.mbl_mw_baro_bosch_set_iir_filter(self.device.board, BaroBoschIirFilter.AVG_4) #2 = AVG_4
        libmetawear.mbl_mw_baro_bosch_write_config(self.device.board)

        #Battery
        self.battery = libmetawear.mbl_mw_settings_get_battery_state_data_signal(self.device.board)
        libmetawear.mbl_mw_datasignal_subscribe(self.battery, None, self.battery_callback)
        libmetawear.mbl_mw_datasignal_read(self.battery)


        # self.acc = libmetawear.mbl_mw_acc_get_packed_acceleration_data_signal(self.device.board)
        # self.gyro = libmetawear.mbl_mw_gyro_bmi160_get_packed_rotation_data_signal(self.device.board)
        # self.mag = libmetawear.mbl_mw_mag_bmm150_get_packed_b_field_data_signal(self.device.board)
        # self.pres = libmetawear.mbl_mw_baro_bosch_get_pressure_data_signal(self.device.board)

        self.acc = libmetawear.mbl_mw_acc_get_acceleration_data_signal(self.device.board)
        self.gyro = libmetawear.mbl_mw_gyro_bmi160_get_rotation_data_signal(self.device.board)
        self.mag = libmetawear.mbl_mw_mag_bmm150_get_b_field_data_signal(self.device.board)
        self.pres = libmetawear.mbl_mw_baro_bosch_get_pressure_data_signal(self.device.board)

        self.acc_logger = create_voidp(lambda fn: libmetawear.mbl_mw_datasignal_log(self.acc, None, fn), resource = "acc_logger")
        self.gyro_logger = create_voidp(lambda fn: libmetawear.mbl_mw_datasignal_log(self.gyro, None, fn), resource = "gyro_logger")
        self.mag_logger = create_voidp(lambda fn: libmetawear.mbl_mw_datasignal_log(self.mag, None, fn), resource = "mag_logger")
        self.pres_logger = create_voidp(lambda fn: libmetawear.mbl_mw_datasignal_log(self.pres, None, fn), resource = "pres_logger")
        '''
        signals = (c_void_p * 1)()
        signals[0] = gyro
        #signals[1] = mag
        #signals[2] = pres
        libmetawear.mbl_mw_dataprocessor_fuser_create(gyro, signals, 1, None, fn_wrapper)
        e.wait()
        '''
        #libmetawear.mbl_mw_datasignal_subscribe(self.processor, None, self.callback)

        # libmetawear.mbl_mw_datasignal_subscribe(self.acc, None, self.acc_callback)
        # libmetawear.mbl_mw_datasignal_subscribe(self.gyro, None, self.gyro_callback)
        # libmetawear.mbl_mw_datasignal_subscribe(self.mag, None, self.mag_callback)
        # libmetawear.mbl_mw_datasignal_subscribe(self.pres, None, self.pres_callback)
        libmetawear.mbl_mw_logger_subscribe(self.acc_logger, None, self.acc_callback)
        libmetawear.mbl_mw_logger_subscribe(self.gyro_logger, None, self.gyro_callback)
        libmetawear.mbl_mw_logger_subscribe(self.mag_logger, None, self.mag_callback)
        libmetawear.mbl_mw_logger_subscribe(self.pres_logger, None, self.pres_callback)


    def start(self):
        self.acc_samples = self.gyro_samples = self.mag_samples = self.pres_samples = 0

        libmetawear.mbl_mw_logging_clear_entries(self.device.board)

        libmetawear.mbl_mw_logging_start(self.device.board, 0)  #Logging Purposes

        libmetawear.mbl_mw_gyro_bmi160_enable_rotation_sampling(self.device.board)
        libmetawear.mbl_mw_acc_enable_acceleration_sampling(self.device.board)
        libmetawear.mbl_mw_mag_bmm150_enable_b_field_sampling(self.device.board)


        libmetawear.mbl_mw_gyro_bmi160_start(self.device.board)
        libmetawear.mbl_mw_acc_start(self.device.board)
        libmetawear.mbl_mw_mag_bmm150_start(self.device.board)
        libmetawear.mbl_mw_baro_bosch_start(self.device.board)

    def stop(self):
        #e = Event()
        #events.append(e)

        libmetawear.mbl_mw_acc_stop(self.device.board)
        libmetawear.mbl_mw_gyro_bmi160_stop(self.device.board)
        libmetawear.mbl_mw_mag_bmm150_stop(self.device.board)
        libmetawear.mbl_mw_baro_bosch_stop(self.device.board)
        #e.set()
        libmetawear.mbl_mw_acc_disable_acceleration_sampling(self.device.board)
        libmetawear.mbl_mw_gyro_bmi160_disable_rotation_sampling(self.device.board)
        libmetawear.mbl_mw_mag_bmm150_disable_b_field_sampling(self.device.board)

        libmetawear.mbl_mw_logging_stop(self.device.board)
        #self.log()

        #e.wait()
        #e.set()

    def log (self):
        #print("File_Name: %s" % file_name)
        self.acc_csv_file = open((folder+('%s_%s__Accelerometer.csv' % (file_name,self.device.address))), 'w')
        self.acc_csv_writer = csv.writer(self.acc_csv_file, delimiter=' ',escapechar=' ', quoting=csv.QUOTE_NONE)


        self.gyro_csv_file = open((folder+('%s_%s__Gyroscope.csv' % (file_name,self.device.address))), 'w')
        self.gyro_csv_writer = csv.writer(self.gyro_csv_file, delimiter=' ',escapechar=' ', quoting=csv.QUOTE_NONE)


        self.mag_csv_file = open((folder+('%s_%s__Magnetometer.csv' % (file_name,self.device.address))), 'w')
        self.mag_csv_writer = csv.writer(self.mag_csv_file, delimiter=' ',escapechar=' ', quoting=csv.QUOTE_NONE)


        self.pres_csv_file = open((folder+('%s_%s__Pressure.csv' % (file_name,self.device.address))), 'w')
        self.pres_csv_writer = csv.writer(self.pres_csv_file, delimiter=' ',escapechar=' ', quoting=csv.QUOTE_NONE)

        self.acc_csv_writer.writerow(["epoch (ms),time (-07:00),elapsed (s),X-Axis (g),Y-Axis (g),Z-Axis (g)"])
        self.gyro_csv_writer.writerow(["epoch (ms),time (-07:00),elapsed (s),X-Axis (deg/s),Y-Axis (deg/s),Z-Axis (deg/s)"])
        self.mag_csv_writer.writerow(["epoch (ms),time (-07:00),elapsed (s),X-Axis (T),Y-Axis (T),Z-Axis (T)"])
        self.pres_csv_writer.writerow(["epoch (ms),time (-07:00),elapsed (s),Pressure (Pa)"])


        e = Event()
        def progress_update_handler(context, entries_left, total_entries):
            if (entries_left == 0):
                e.set()
        
        fn_wrapper = FnVoid_VoidP_UInt_UInt(progress_update_handler)
        download_handler = LogDownloadHandler(context = None, \
            received_progress_update = fn_wrapper, \
            received_unknown_entry = cast(None, FnVoid_VoidP_UByte_Long_UByteP_UByte), \
            received_unhandled_entry = cast(None, FnVoid_VoidP_DataP))

        #libmetawear.mbl_mw_logger_subscribe(logger, None, callback)
        libmetawear.mbl_mw_logging_download(self.device.board, 0, byref(download_handler))
        e.wait()

        sleep(1.5)
        self.acc_csv_file.close()
        self.gyro_csv_file.close()
        self.mag_csv_file.close()
        self.pres_csv_file.close()



    def close(self,events):
        e = Event()
        events.append(e)

        libmetawear.mbl_mw_datasignal_unsubscribe(self.acc)
        libmetawear.mbl_mw_datasignal_unsubscribe(self.gyro)
        libmetawear.mbl_mw_datasignal_unsubscribe(self.mag)
        libmetawear.mbl_mw_datasignal_unsubscribe(self.pres)

        e.set()


    def disconnect_handler(self):
        print("%s has Disconnected .... Trying to Reconnect" % (self.device.address))
        while True:
            try:
                self.device.connect()
            except Exception as e:
                print(str(e))
                print("Unable To Connect ... Trying Again")
                sleep(1.0)
            else:
                print("Reconnection to %s was Successful!!" % (self.device.address))
                break


# Reseting Bluetooth Ports (Soft Reset)
# Equivalent of the _IO('U', 20) constant in the linux kernel.
USBDEVFS_RESET = ord('U') << (4*2) | 20


A = "C7:E1:38:1F:C0:DE"
B = "F4:04:52:A2:CB:59"
C = "E3:62:1F:8B:81:B7"
D = "E8:9C:A5:A3:8A:60"
E = "F9:0E:1C:DA:D4:1D"
F = "CD:A5:4D:78:A1:B4"
G = "EF:AA:47:DC:45:44"


file_name = None
folder = None

while 1:
    print ("TCPServer Waiting for client on port 5000")
    client_socket, address = server_socket.accept()
    print ("I got a connection from ", address)
    #data = ((client_socket.recv(512)).decode('UTF'))[2:]
    #data = input ( "SEND( TYPE q or Q to Quit):" )
    data = None
    #message_to_send = "bye\r\n".encode("UTF-8")
    #client_socket.send(len(message_to_send).to_bytes(2, byteorder='big'))
    #client_socket.send(data.encode("UTF-8"))
    while (data != 'quit'):
        try:
            ready_to_read, ready_to_write, in_error = \
                select.select([client_socket,], [client_socket,], [], 5)
        except Exception as e:
            print(str(e))
            print("An Error Occured...Disconnecting")
            break
        else:
            print("Reached Here")
            data = ((client_socket.recv(512)).decode('UTF'))[2:]
            if (data == 'config'): #Main Code that connects to devices
                #Placeholder for main_data.py
                reset_bl()  #This only resets the USB BL dongle and not the imbeedded BL 
                sleep(0.5)

                sensors = [A,B,C,D,E,F,G] 
                states = []
                print ("RECIEVED:" , data)
                ###
                for i in range(len(sensors)):
                    if i<4:
                        hci = 'hci0' #hci0  00:1A:7D:DA:71:15 --BL USB
                    else:
                        hci = 'hci1' #hci1  DC:A6:32:38:26:18 --Internal BL
                    d = MetaWear(sensors[i], hci_mac = hci)
                    while True:
                        try:
                            d.connect()
                        except Exception as e:
                            print(str(e))
                            print("Unable To Connect ... Trying Again")
                            sleep(1.0)
                        else:
                            break
                    print("Connected to " + d.address + " -BLE Dongle " + hci + "\n")
                    states.append(State(d))

                    while True:
                        try:
                            #Configuration
                            thread = myThread("Configuration %s" %(states[i].device.address), states[i], State.config)
                            thread.start()
                            thread.join()
                            sleep(1.0)
                            #Turn LED -- Trun ON when needed

                            #thread = myThread("LED %s" %(states[i].device.address), states[i], State.led)
                            #thread.start()
                            #thread.join()

                        except Exception as e:
                            print(str(e))
                            print("Failed Configuration ... Trying Again")
                            sleep(1.0)
                        else:
                            break
                ###
                client_socket.send(("true\r\n").encode('UTF-8'))

            elif(data == 'name'):
                print ("RECIEVED:" , data)
                ###
                file_name = ((client_socket.recv(512)).decode('UTF'))[2:]
                print ("File Name:" , file_name)

                items = file_name.split('_')
                folder = ("Data/%s/%s/%s/" % (items[2],items[0],items[1]))
                create_folder(folder)
                ###
                client_socket.send(("true\r\n").encode('UTF-8'))

            elif (data == 'start'):
                print ("RECIEVED:" , data)
                #client_socket.send(("hello").encode('UTF'))
                ###
                Threads = []
                for s in states:
                    #print ("ON %s" %s.device.address)
                    thread = myThread("Turning On Device %s" %(s.device.address), s, State.start)
                    thread.start()
                    Threads.append(thread)

                for t in Threads:
                    t.join()
                ###
                client_socket.send(("true\r\n").encode('UTF-8'))

            elif (data == 'stop'):
                print ("RECIEVED:" , data)
                ### 
                Threads = []
                for s in states:
                    #print ("ON %s" %s.device.address)
                    thread = myThread("Stopping Recording%s" %(s.device.address), s, State.stop)
                    thread.start()
                    Threads.append(thread)
                
                for t in Threads:
                    t.join()

                Threads = []
                for s in states:
                    #print ("ON %s" %s.device.address)
                    thread = myThread("Downloading Log %s" %(s.device.address), s, State.log)
                    thread.start()
                    Threads.append(thread)

                for t in Threads:
                    t.join()


                print("Total Samples Received")
                for s in states:
                    print("%s -> %d" % (s.device.address, (s.acc_samples+s.gyro_samples+s.mag_samples+s.pres_samples)))
                ###
                client_socket.send(("true\r\n").encode('UTF-8'))

            elif(data == 'close'):
                print ("RECIEVED:" , data)
                ###
                print("Resetting devices")
                events = []
                for s in states:
                    e = Event()
                    events.append(e)

                    s.close(events)

                    s.device.on_disconnect = lambda s: e.set() 
                    libmetawear.mbl_mw_debug_reset(s.device.board)
                    libmetawear.mbl_mw_metawearboard_free(s.device.board)
                    libmetawear.mbl_mw_debug_disconnect(s.device.board)
                    

                for e in events:
                    e.wait()
                ###
                client_socket.send(("true\r\n").encode('UTF-8'))
                
            else :
                print ("RECIEVED:" , data)
                #sleep(2.0)
                client_socket.send(("true\r\n").encode('UTF-8'))
                print ("OKAY")

        '''
        data = input ( "SEND( TYPE q or Q to Quit):" )
        if (data == 'Q' or data == 'q'):
            client_socket.send (data.encode('ascii'))
            client_socket.close()
            break
        else:
            client_socket.send(data.encode('ascii'))

        data = (client_socket.recv(512)).decode('ascii')
        '''
    client_socket.close()
    print ("CONNECTION DISCONNECTED\n")
