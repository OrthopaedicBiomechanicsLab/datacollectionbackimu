import pandas as pd
import time, datetime, csv, signal


sensor = "abdo_session1_LayingDownExtention(2)1_C7:E1:38:1F:C0:DE"

#sensor = "E3:62:1F:8B:81:B7"
dfa = pd.read_csv("Data/"+sensor+"__Accelerometer.csv") #Reading the dataset in a dataframe using Pandas
dfg = pd.read_csv("Data/"+sensor+"__Gyroscope.csv") #Reading the dataset in a dataframe using Pandas
dfm = pd.read_csv("Data/"+sensor+"__Magnetometer.csv") #Reading the dataset in a dataframe using Pandas
dfp = pd.read_csv("Data/"+sensor+"__Pressure.csv") #Reading the dataset in a dataframe using Pandas

#df.head(5) #This will display the first 5 lines of the imported data set
#df2.head(5)

#print(df)
test1a = pd.merge_asof(dfa, dfg, on='epoch  (ms)')
test1b = test1a.drop(columns=['time  (-07:00)_y','elapsed  (s)_y'])
#print(n)

test2a = pd.merge_asof(test1b, dfm, on='epoch  (ms)')
test2b = test2a.drop(columns=['time  (-07:00)','elapsed  (s)'])


test3a = pd.merge_asof(test2b, dfp, on='epoch  (ms)')
test3b = test3a.drop(columns=['time  (-07:00)','elapsed  (s)'])

test4 = test3b.rename(columns={'time  (-07:00)_x': 'time  (-07:00)','elapsed  (s)_x': 'elapsed  (s)'})
#test4.to_csv('Data/'+sensor+'_FULL.csv', index=False)
print(test4)

'''
import os

folder = "Data/gee"





def create_folder(path):
	#Create folder directory
    """Change the owner of the file to SUDO_UID"""

    if not os.path.exists(path):
    	os.makedirs(path, mode=0o777)

    uid = os.environ.get('SUDO_UID')
    gid = os.environ.get('SUDO_GID')
    if uid is not None:
        os.chown(path, int(uid), int(gid))

create_folder(folder)

name = "_"+sensor+"__Accelerometer.csv"

items = name.split('_')

print(items[1])
'''