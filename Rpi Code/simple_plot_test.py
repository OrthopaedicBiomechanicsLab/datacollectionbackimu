from bokeh.layouts import layout , column
from bokeh.models.widgets import Panel, Tabs
from numpy import pi, arange, sin, cos
from bokeh.plotting import output_file, figure, show
from bokeh.io import curdoc
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, Grid, LinearAxis, MultiLine, Plot

output_file("slider.html")

x = arange(-2*pi, 2*pi, 0.1)
# your different functions
y1 = sin(x)
y2 = cos(x)
p = []
tab=[]



for sensor in [A,]

for idx, (y , color) in enumerate(zip([y1,y2] , ["red","blue"])):
# building a tab per function/plot
    p.append(figure(plot_width=300, plot_height=300))
    p[idx].multi_line(xs=, ys=, line_color=color)

    #tab1 = Panel(child=p1, title="sinus")

    #p2 = figure(plot_width=300, plot_height=300)
    #p2.line(x, y2, color="blue")
    #tab2 = Panel(child=p2, title="cosinus")
    '''
    # aggregating and plotting
    tabs = Tabs(tabs=[tab1, tab2])
    show(tabs,browser=None)
    '''

    #plots_layout = [x,y1]
    #tables_layout = [y1,y2]
    #source = ColumnDataSource(sample)

    #p = figure()
    #p.circle(x='TOTAL_TONS', y='AC_ATTACKING',
    #         source=source,
    #         size=10, color='green')
    p[idx].title.text = 'Attacking Aircraft and Munitions Dropped'
    p[idx].xaxis.axis_label = 'Tons of Munitions Dropped'
    p[idx].yaxis.axis_label = 'Number of Attacking Aircraft'

tab_layout = layout([[p[0]],[p[1]],[p[2]],[p[3]]],sizing_mode="stretch_both")
#tab2_layout = layout([[p[1], p[0]]],sizing_mode="stretch_both")

# Create Tabs
tab.append(Panel(child=tab1_layout, title='Tab1'))
tab2 = Panel(child=tab2_layout, title='Tab2')
# Put the Panels in a Tabs object 
tabs = Tabs(tabs=[tab1, tab2], height=500)  
#curdoc().add_root(tabs)
show(tabs)  