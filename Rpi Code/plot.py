from matplotlib import interactive, pyplot as plt
import numpy as np
import math #needed for definition of pi
import pandas as pd
import time, datetime, csv, signal


from bokeh.layouts import layout , column
from bokeh.models.widgets import Panel, Tabs
from numpy import pi, arange, sin, cos
from bokeh.plotting import output_file, figure, show
from bokeh.io import curdoc
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, Grid, LinearAxis, MultiLine, Plot


tabs=[]
name = None
session_name = "abdo_testsession_#13 Other"
items = session_name.split('_')
folder = "Data/"+items[2]+"/"+items[0]+"/"+items[1]+"/"

A = "C7:E1:38:1F:C0:DE"
B = "F4:04:52:A2:CB:59"
C = "E3:62:1F:8B:81:B7"
D = "E8:9C:A5:A3:8A:60"
E = "F9:0E:1C:DA:D4:1D"
F = "CD:A5:4D:78:A1:B4"
G = "EF:AA:47:DC:45:44"

sensor = "C7:E1:38:1F:C0:DE"
for idx , (sensor,sensor_name) in enumerate(zip([A,B,C,D,E,F,G],['Left Wrist (A)','Left Shoulder (B)','Right Shoulder (C)',
	'Upper Back (D)','Lower Back (E)','Left Thight (F)','Right Ankle (G)'])):
	'''###
	MERGE CODE
	'''###
	dfa = pd.read_csv(folder+session_name+"_"+sensor+"__Accelerometer.csv") #Reading the dataset in a dataframe using Pandas
	dfg = pd.read_csv(folder+session_name+"_"+sensor+"__Gyroscope.csv") #Reading the dataset in a dataframe using Pandas
	dfm = pd.read_csv(folder+session_name+"_"+sensor+"__Magnetometer.csv") #Reading the dataset in a dataframe using Pandas
	dfp = pd.read_csv(folder+session_name+"_"+sensor+"__Pressure.csv") #Reading the dataset in a dataframe using Pandas

	#df.head(5) #This will display the first 5 lines of the imported data set
	#df2.head(5)

	test1a = pd.merge_asof(dfa, dfg, on='epoch  (ms)')
	test1b = test1a.drop(columns=['time  (-07:00)_y','elapsed  (s)_y'])

	test2a = pd.merge_asof(test1b, dfm, on='epoch  (ms)')
	test2b = test2a.drop(columns=['time  (-07:00)','elapsed  (s)'])

	test3a = pd.merge_asof(test2b, dfp, on='epoch  (ms)')
	test3b = test3a.drop(columns=['time  (-07:00)','elapsed  (s)'])

	df = test3b.rename(columns={'time  (-07:00)_x': 'time  (-07:00)','elapsed  (s)_x': 'elapsed  (s)'}) #test4

	'''###
	Plot Code
	'''###

	source = ColumnDataSource(df)

	#print(df['X-Axis  (g)'].values)

	#print(source['id'])

	#p = figure()
	#p.multi_line(xs=[df['elapsed  (s)'].values]*2, ys=[df[sensor_type].values for sensor_type in ['X-Axis  (g)','Y-Axis  (g)']],
	#         color='green')
	#show(p)


	acc_col_label = ['X-Axis  (g)','Y-Axis  (g)','Z-Axis  (g)']
	gyro_col_label =['X-Axis  (deg/s)','Y-Axis  (deg/s)','Z-Axis  (deg/s)']
	mag_col_label = ['X-Axis  (T)','Y-Axis  (T)','Z-Axis  (T)']
	pres_col_label = ['Pressure  (Pa)']

	sensor_type_col_labels = [acc_col_label,gyro_col_label,mag_col_label,pres_col_label]

	acc_colors = ['#FFA07A','#FF0000','#DC143C']
	gyro_colors = ['#90EE90','#7CFC00', '#008000']
	mag_colors = ['#ADD8E6','#00BFFF','#4682B4']
	pres_colors = ['#CCCC00']

	sensor_type_color = [acc_colors,gyro_colors,mag_colors,pres_colors]

	data_title = ["Accelerometer Data","Gyroscope Data","Magnetometer Data","Pressure Data"]
	data_y_labels = ["Gravity of Earth (g)","Degrees per Seconds (deg/s)","Magnetic Field Strength (uT)","Pressure (Pa)"]

	p = []
	output_file(session_name.replace("#","")+".html")

	for idx, (sensor_type , color, title, y_label) in enumerate(zip(sensor_type_col_labels ,sensor_type_color,data_title ,data_y_labels)):
	# building a tab per function/plot
	    p.append(figure(plot_width=300, plot_height=235))
	    if idx == 3:
	    	num_sensor_types = 1
	    else:
	    	num_sensor_types = 3

	    for sensor_type_xyz, color_xyz in zip(sensor_type,color): 
		    p[idx].line(x = 'elapsed  (s)',
		    	y = sensor_type_xyz,
		    	source = source,
		    	legend_label= sensor_type_xyz, 
		    	line_color=color_xyz)

	    #tab1 = Panel(child=p1, title="sinus")

	    #p2 = figure(plot_width=300, plot_height=300)
	    #p2.line(x, y2, color="blue")
	    #tab2 = Panel(child=p2, title="cosinus")
	    '''
	    # aggregating and plotting
	    tabs = Tabs(tabs=[tab1, tab2])
	    show(tabs,browser=None)
	    '''

	    #plots_layout = [x,y1]
	    #tables_layout = [y1,y2]
	    #source = ColumnDataSource(sample)

	    #p = figure()
	    #p.circle(x='TOTAL_TONS', y='AC_ATTACKING',
	    #         source=source,
	    #         size=10, color='green')
	    p[idx].title.text = title
	    p[idx].xaxis.axis_label = 'Time (s)'
	    p[idx].yaxis.axis_label = y_label

	tab_layout = layout([[p[0]],[p[1]],[p[2]],[p[3]]],sizing_mode="stretch_width")#stretch_both"
	tabs.append(Panel(child=tab_layout, title=sensor_name))


all_tabs = Tabs(tabs=tabs, height=400)  
#curdoc().add_root(tabs)
show(all_tabs) 

########################################################OLD CODE
	# figure, axes = plt.subplots(4)
	# plt.figure(idx)
	# #sensor = "E3:62:1F:8B:81:B7"
	# #dfa = pd.read_csv("Data/"+sensor+"__Accelerometer.csv") #Reading the dataset in a dataframe using Pandas
	# #dfg = pd.read_csv("Data/"+sensor+"__Gyroscope.csv") #Reading the dataset in a dataframe using Pandas
	# #dfm = pd.read_csv("Data/"+sensor+"__Magnetometer.csv") #Reading the dataset in a dataframe using Pandas
	# #dfp = pd.read_csv("Data/"+sensor+"__Pressure.csv") #Reading the dataset in a dataframe using Pandas
	# #df2 = pd.read_csv("Data/E3:62:1F:8B:81:B7__Gyroscope.csv") #Reading the dataset in a dataframe using Pandas

	# #df = pd.read_csv("Data/"+sensor+"_FULL.csv") #Reading the dataset in a dataframe using Pandas
	# ax = plt.gca()

	# #Grid
	# a1 = plt.subplot2grid((4,1),(0,0),colspan = 1)
	# a2 = plt.subplot2grid((4,1),(1,0), colspan = 1)
	# a3 = plt.subplot2grid((4,1),(2,0), colspan = 1)
	# a4 = plt.subplot2grid((4,1),(3,0), colspan = 1)

	# #Acc
	# df.plot(x='elapsed  (s)',y='X-Axis  (g)',color = '#FFA07A',ax=a1)
	# df.plot(x='elapsed  (s)',y='Y-Axis  (g)',color = '#FF0000',ax=a1)
	# df.plot(x='elapsed  (s)',y='Z-Axis  (g)',color = '#DC143C',ax=a1)

	# #Gyro
	# df.plot(x='elapsed  (s)',y='X-Axis  (deg/s)',color = '#90EE90',ax=a2)
	# df.plot(x='elapsed  (s)',y='Y-Axis  (deg/s)',color = '#7CFC00',ax=a2)
	# df.plot(x='elapsed  (s)',y='Z-Axis  (deg/s)',color = '#008000',ax=a2)

	# #Mag
	# df.plot(x='elapsed  (s)',y='X-Axis  (T)',color = '#ADD8E6',ax=a3)
	# df.plot(x='elapsed  (s)',y='Y-Axis  (T)',color = '#00BFFF',ax=a3)
	# df.plot(x='elapsed  (s)',y='Z-Axis  (T)',color = '#4682B4',ax=a3)

	# #Pres
	# df.plot(x='elapsed  (s)',y='Pressure  (Pa)',color = '#CCCC00',ax=a4)


	# #Blue: 
	# #ADD8E6
	# #00BFFF
	# #4682B4

	# #Red
	# #FFA07A
	# #FF0000
	# #DC143C

	# #Green
	# #90EE90
	# #7CFC00
	# #008000

	# #Cyan
	# #40E0D0

	# #plt.legend(loc=2, prop={'size': 6})

	# a1.set_title("Accelerometer Data")
	# a1.set_ylabel("Gravity of Earth (g)") #Acc
	# #a1.set_xlabel("") #Acc


	# a2.set_title("Gyroscope Data")
	# a2.set_ylabel("Degrees per Seconds (deg/s)") #Acc
	# #a2.set_xlabel("") #Acc

	# a3.set_title("Magnetometer Data")
	# a3.set_ylabel("Magnetic Field Strength (T)") #Acc
	# #a3.set_xlabel("") #Acc

	# a4.set_title("Pressure Data")
	# a4.set_ylabel("Pressure (Pa)") #Acc
	# #a4.set_xlabel("") #Acc



	# #plt.ylabel("Degrees per Seconds (deg/s)") #gyro
	# #plt.ylabel("Magnetic Field Strength (T)") #mag

	# plt.subplots_adjust(hspace = 0.75)

	# #plt.xlabel("Time (s)")
	# plt.tight_layout()
	# if idx ==0:
	# 	interactive(True)
	# elif idx == 7:
	# 	interactive(False)

##########################################################################OLD CODE
# plt.show()
#input()
#############################################################################
#############################################################################

# output_file("slider.html")

# x = arange(-2*pi, 2*pi, 0.1)
# # your different functions
# y1 = sin(x)
# y2 = cos(x)
# p = []
# tab=[]


# for sensor in [A,]



# tab_layout = layout([[p[0]],[p[1]],[p[2]],[p[3]]],sizing_mode="stretch_both")
# #tab2_layout = layout([[p[1], p[0]]],sizing_mode="stretch_both")

# # Create Tabs
# tab.append(Panel(child=tab1_layout, title='Tab1'))
# tab2 = Panel(child=tab2_layout, title='Tab2')
# # Put the Panels in a Tabs object 
# tabs = Tabs(tabs=[tab1, tab2], height=500)  
# #curdoc().add_root(tabs)
# show(tabs)  






'''

	x = np.arange(0, math.pi*2, 0.05)
	y = np.tan(x)
	plt.plot(x,y)
	plt.xlabel("angle")
	plt.ylabel("Tan value")
	plt.title('Tan wave')
	plt.show()
	'''